Name:             bean-validation-api
Version:          1.1.0
Release:          13
Summary:          Bean Validation API (JSR 349)
License:          ASL 2.0
URL:              https://github.com/beanvalidation/beanvalidation-api/
Source0:          https://github.com/beanvalidation/beanvalidation-api/archive/1.1.0.Final.tar.gz
BuildArch:        noarch

BuildRequires:    java-devel, maven-local, mvn(org.apache.felix:maven-bundle-plugin), mvn(org.apache.maven.surefire:surefire-testng), mvn(org.testng:testng)
Provides:         %{name}-javadoc%{?_isa} %{name}-javadoc
Obsoletes:        %{name}-javadoc

%description
This module includes Bean Validation (JSR-349) API.

%prep
%autosetup -n validation-1.1.0.Final -p1

%pom_xpath_remove "pom:build/pom:plugins/pom:plugin[pom:artifactId='maven-javadoc-plugin']/pom:executions"
%pom_remove_plugin :maven-source-plugin

find src/test/java -name "ValidationTest.java" -print -delete

%mvn_file : %{name}

%build
%mvn_build

%install
%mvn_install

%files -f .mfiles
%license license.txt
%{_javadocdir}/%{name}/*

%changelog
* Wed Nov 9 2022 liyanan <liyanan32@h-partners.com>  - 1.1.0-13
- Change source

* Tue Jan 05 2021 Ge Wang <wangge20@huawei.com> - 1.1.0-12
- Modify homepage url

* Fri Dec 13 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.1.0-11
- Package init
